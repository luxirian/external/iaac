data "aws_instance" "instance" {
  for_each = local.ec2_ids
  instance_id = each.key
}

resource "null_resource" "tagger" {
  for_each = local.ec2_ids
  triggers = {
    tags = local.formatted_tags
    }
  
  provisioner "local-exec" {
    command = "aws ec2 create-tags --region us-east-1 --resources ${each.key} --tags ${local.formatted_tags}"
  }
}