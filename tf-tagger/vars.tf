variable "instance_ids" {
  type = list(string)
  default = ["replace with instance id's"]
}

variable "bucket" {
  type = string
  default = ""
}

variable "bucket_key" {
  type = string
  default = ""
}

variable "profile" {
  type = string
  default = ""
}

variable "region" {
  type = string
  default = ""
}

# variable "tag_map" {
#   default = {
#     "tag_1" = "HELLOWORLD_1"
#     "tag_2" = "HELLOWORLD_2"
#   }
# }
