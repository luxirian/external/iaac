terraform {
  backend "s3" {
    bucket = var.bucket
    key    = var.bucket_key
    region = var.region
    profile = var.profile
  }
}
