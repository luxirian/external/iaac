locals {
  ec2_ids = toset(var.instance_ids)
}

locals {
  formatted_tags = join(" ", formatlist("Key='%s',Value='%s'", keys(local.tags), values(local.tags))) #change to var.tags_map to use variable
}

locals {
  tags = {
    some_key = "${terraform.workspace == "default" ? "test-1-test" : "test-2-test"}"
  }
}